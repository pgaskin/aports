# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer:  Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=perl-io-compress
_pkgname=IO-Compress
pkgver=2.092
pkgrel=0
pkgdesc="Perl compression library"
url="https://metacpan.org/release/IO-Compress"
arch="noarch"
license="GPL-1.0-or-later OR Artistic-1.0-Perl"
depends="perl perl-compress-raw-bzip2 perl-compress-raw-zlib"
makedepends="perl-dev"
source="https://cpan.metacpan.org/authors/id/P/PM/PMQS/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

prepare() {
	default_prepare
	export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')
	PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
}

build() {
	export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')
	make
}

check() {
	export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')
	make test
}

package() {
	export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')
	make DESTDIR="$pkgdir" install

	# conflicts with core perl
	rm "$pkgdir"/usr/bin/zipdetails
	rmdir "$pkgdir"/usr/bin 2>/dev/null || true

	# creates file collision among perl modules
	find "$pkgdir" \( -name perllocal.pod -o -name .packlist \) -delete

	# man pages are already provided by perl-doc
	rm -rf "$pkgdir"/usr/share/man
}

sha512sums="6142350dcad30ed85a6e9934c0572de331f69064340e5ffc964a730a20378aee66024bf90d79f5803eb0908b888ceffd8cb0b2306ae8f9987cc8903147255832  IO-Compress-2.092.tar.gz"
