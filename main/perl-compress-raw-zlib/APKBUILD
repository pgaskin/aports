# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer:  Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=perl-compress-raw-zlib
_realname=Compress-Raw-Zlib
pkgver=2.092
pkgrel=0
pkgdesc="Perl low-level interface to zlib compression library"
url="https://metacpan.org/release/Compress-Raw-Zlib"
arch="all"
license="GPL-1.0-or-later OR Artistic-1.0-Perl"
depends="perl"
makedepends="perl-dev"
source="http://search.cpan.org/CPAN/authors/id/P/PM/PMQS/$_realname-$pkgver.tar.gz"
builddir="$srcdir/$_realname-$pkgver"

build() {
	export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')
	PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
	make
}

check() {
	export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	find "$pkgdir" \( -name perllocal.pod -o -name .packlist \) -delete

	# man pages are already provided by perl-doc
	rm -rf "$pkgdir"/usr/share/man
}

sha512sums="edc07181535b592c21980d034dbdf197a7d7b20ed96cdaf36cc66147edb885305f63dd4bfef485ad12c3a3af1ea0e7b8776bcc3fc44178e585fd3dbe7a391658  Compress-Raw-Zlib-2.092.tar.gz"
